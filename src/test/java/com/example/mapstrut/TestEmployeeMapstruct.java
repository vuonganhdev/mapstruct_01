package com.example.mapstrut;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class TestEmployeeMapstruct {

  @InjectMocks
  EmployeeMapperImpl employeeMapper;
  @Test
  void employeeMapstruct_Successfully(){
    EmployeeEntity employeeEntity = EmployeeEntity.builder()
        .id(12)
        .name("Vuong Anh")
        .build();
    EmployeeDto dto = employeeMapper.mapToDto(employeeEntity);
    Assertions.assertEquals(12, dto.getId());
    Assertions.assertEquals("Vuong Anh", dto.getName());
  }

}

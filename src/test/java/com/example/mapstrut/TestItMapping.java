package com.example.mapstrut;

import com.example.mapstrut.It.ItDto;
import com.example.mapstrut.It.ItEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
class TestItMapping {

  @InjectMocks
  ItMappingImpl itMapping;
  @Test
  void employeeMapstruct_Successfully(){
    ItEntity employeeEntity = ItEntity.builder()
        .id(12)
        .name("Vuong Anh")
        .address("Tuyen Quang")
        .salary(0)
        .build();
    ItDto dto = itMapping.mapToDto(employeeEntity);
    Assertions.assertEquals(12, dto.getId());
    Assertions.assertEquals("Vuong Anh", dto.getName());
    Assertions.assertEquals("Tuyen Quang", dto.getHome());
    Assertions.assertEquals("Muoi trieu", dto.getSalary());
  }

}

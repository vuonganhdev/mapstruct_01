package com.example.mapstrut;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
public class EmployeeDto {
  private int id;
  private String name;

}

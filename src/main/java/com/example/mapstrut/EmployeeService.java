package com.example.mapstrut;

import org.springframework.stereotype.Service;

@Service
public class EmployeeService {

  private final EmployeeMapper employeeMapper;

  public EmployeeService(EmployeeMapper employeeMapper) {
    this.employeeMapper = employeeMapper;
  }


  public EmployeeDto mapEmployee(EmployeeEntity employeeEntity){
    return employeeMapper.mapToDto(employeeEntity);
  }
}

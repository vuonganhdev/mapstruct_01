package com.example.mapstrut;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MapstrutApplication {

	public static void main(String[] args) {
		SpringApplication.run(MapstrutApplication.class, args);
	}

}

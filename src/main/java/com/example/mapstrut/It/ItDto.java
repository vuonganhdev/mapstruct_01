package com.example.mapstrut.It;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@Builder
@AllArgsConstructor
public class ItDto {
  private int id;
  private String name;
  private String home;
  private String salary;

}

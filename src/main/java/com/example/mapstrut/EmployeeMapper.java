package com.example.mapstrut;

import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface EmployeeMapper {

  EmployeeDto mapToDto(EmployeeEntity employeeEntity);

}

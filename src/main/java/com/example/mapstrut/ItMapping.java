package com.example.mapstrut;

import com.example.mapstrut.It.ItDto;
import com.example.mapstrut.It.ItEntity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface ItMapping {

  @Mapping(target = "home", source = "address")
  @Mapping(target = "salary", source = "salary", qualifiedByName = "salaryMap")
  ItDto mapToDto(ItEntity itEntity);

  @Named("salaryMap")
  public static String salaryMap(int salary){
    if(salary == 0){
      return "Muoi trieu";
    }
    return "Hai muoi trieu";
  }

}

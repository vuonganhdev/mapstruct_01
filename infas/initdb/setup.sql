CREATE USER admin_av WITH PASSWORD 'admin';
GRANT ALL PRIVILEGES ON DATABASE general_db to admin_av;
ALTER USER admin_av CREATEROLE;

CREATE ROLE admin NOSUPERUSER NOCREATEDB NOCREATEROLE NOINHERIT LOGIN PASSWORD 'admin';
